const config = require('./config/tailwind.js')

module.exports = {
  theme: {
    ...config.theme,
  },
  purge: [],
  darkMode: false,
  variants: {
    extend: {},
  },
  plugins: [],
}
