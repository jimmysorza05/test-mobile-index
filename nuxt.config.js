import buildModules from './config/buildModules'
import head from './config/head'
import css from './config/css'
import plugins from './config/plugins'
import modules from './config/modules'
import env from './config/env'
import axios from './config/axios'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head,
  /**
   * Global Env Varialbes
   */
  env,

  // Global CSS: https://go.nuxtjs.dev/config-css
  css,

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins,

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules,

  // Modules: https://go.nuxtjs.dev/config-modules
  modules,

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios,

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },
}
