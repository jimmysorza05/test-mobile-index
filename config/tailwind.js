module.exports = {
  theme: {
    backgroundColor: (theme) => ({
      ...theme('colors'),
      primary: '#FFFFFF',
      secondary: '#FAFAFA',
      black: '#101010',
    }),
    borderRadius: {
      DEFAULT: '10px',
      md: '0.375rem',
    },
    extend: {
      spacing: {
        '30px': '30px',
        '60px': '60px',
        '100px': '100px',
        '140px': '140px',
        '160px': '160px',
      },
      fontFamily: {
        primary: ['Tinos', 'tinos'],
        secondary: ['Roboto', 'roboto'],
      },
    },
  },
}
