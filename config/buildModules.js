export default [
  // https://go.nuxtjs.dev/eslint
  '@nuxtjs/eslint-module',
  // https://go.nuxtjs.dev/stylelint
  '@nuxtjs/stylelint-module',
  // https://go.nuxtjs.dev/tailwindcss
  '@nuxtjs/tailwindcss',
]
